# Tools to Organize and Build UHD Images

This repo maintains compatible configurations of uhd-fpga and theseus-cores.

To use this code in a RFNoC build, point the UHD Out-Of-Tree build to
one (or more) of the subdirectories here to build the IP and include
relevant fpga-src for these modules as the `-I` include-dirs argument.

The valid Out-Of-Tree target modules in theseus-cores are:
- src/theseus-cores/dsp-utils
- src/theseus-cores/m2_channelizer

Many of the noc blocks have configurable parameters. It is recommended to build the FPGA image by specifing a YML file with the desired parameters (see the `examples` subfolder here for configuration examples).

# Using Git Submodules

To clone this repo fresh: `git clone --recursive https://gitlab.com/theseus-cores/theseus-uhd-builder.git`

This will properly clone into the corresponding git submodules using the recursive flag.

If you've clone the repo and need to initialize or re-initialize the submodules, going to the last known state for each submodule repo: `git submodule update --init`

If you're on a branch (i.e., UHD-3.14), and would like to pull in new code: `git submodule update --remote`

# Example RFNoC Build

To build an RFNOC FPGA image with components from the "dsp-utils" subproject, you can use refer to the example build-config YML [build-configs/basic-dsp-utils.yml](build-configs/basic-dsp-utils.yml).

First, install Vivado 2017.4 as per uhd-fpga installation instructions.

Next, call the `uhd_image_builder.py` script, using a YML configuration and pointing to the corresponding out-of-tree repo location (examples below). These examples are designed to be run straight from the root directory of the theseus-uhd-builder.

Note, these examples are configured for an X300 using the build mode X300_RFNOC_HG. Modify these build targets as needed for the relevant device.

**Basic DSP Utils**

```
python src/uhd-fpga/usrp3/tools/scripts/uhd_image_builder.py -d x300 -t X300_RFNOC_HG -y build-configs/basic-dsp-utils.yml -I src/theseus-cores/fpga-rfnoc/dsp-utils
```

**Basic Channelizer**

```
python src/uhd-fpga/usrp3/tools/scripts/uhd_image_builder.py -d x300 -t X300_RFNOC_HG -y build-configs/basic-channelizer.yml -I src/theseus-cores/fpga-rfnoc/m2_channelizer
```
